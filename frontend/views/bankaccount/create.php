<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BankAccount */

$this->title = 'Create Customer Data';
$this->params['breadcrumbs'][] = ['label' => 'Customer Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bank-account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
