<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Timedata */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="timedata-form">

    <?php $form = ActiveForm::begin([
//'action' => ['pesan/pilih3']
    ]); ?>


    <?php $dataList=ArrayHelper::map(Customer::find()->asArray()->all(), 'id', 'nama');?>
	 <?=$form->field($model, 'customer_id')->dropDownList($dataList,
         ['prompt'=>'-Pilih Penyimpanan-'])->label('Customer') ?>


    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

<?php
echo '<label>Value</label>';
    echo DatePicker::widget([
    'model' => $model, 
    'attribute' => 'value',
    'value' => date('Y-mm-d', strtotime('+2 days')),
    'options' => ['placeholder' => 'Select issue date ...'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true
    ]
]);
?>
<br/><br/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
