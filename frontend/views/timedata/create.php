<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Timedata */

$this->title = Yii::t('app', 'Create Timedata');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Timedatas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timedata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
