<?php

namespace frontend\models;

use Yii;
use app\models\Customer;

/**
 * This is the model class for table "timedata".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $category
 * @property string $value
 */
class Timedata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timedata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['value'], 'safe'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'category' => Yii::t('app', 'Category'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @inheritdoc
     * @return TimedataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TimedataQuery(get_called_class());
    }
}
